extends Position3D

# Constants
# ------------------------------------------------------------------------------
const MOVEMENT_SPEED: float = 20.0
const ZOOM_HI: float = 15.0
const ZOOM_LOW: float = 5.0

# Enumerations
# ------------------------------------------------------------------------------
enum direction { 
	up = 1, 
	down = 2, 
	left = 4, 
	right = 8
}

# Variables
# ------------------------------------------------------------------------------
onready var camera = $Camera

# Overrides
# ------------------------------------------------------------------------------
func _process(delta):
	if Input.is_action_pressed("ui_up"):
		move(direction.up, delta)
	if Input.is_action_pressed("ui_down"):
		move(direction.down, delta)
	if Input.is_action_pressed("ui_left"):
		move(direction.left, delta)
	if Input.is_action_pressed("ui_right"):
		move(direction.right, delta)
		

# Methods
# ------------------------------------------------------------------------------
func move(dir: int, delta: float) -> void:
	var blocked_v: int = direction.up | direction.down;
	var blocked_h: int = direction.left | direction.right
	if dir == blocked_h || dir ==blocked_v :
		return
	var velocity: Vector3 = Vector3.ZERO
	if dir == direction.left:
		velocity -= transform.basis.x
		velocity -= transform.basis.z
	if dir == direction.right:
		velocity += transform.basis.x
		velocity += transform.basis.z
	if dir == direction.up:
		velocity -= transform.basis.z
		velocity += transform.basis.x	
	if dir == direction.down:
		velocity += transform.basis.z
		velocity -= transform.basis.x
	velocity = velocity.normalized()
	translation += velocity * delta * MOVEMENT_SPEED
	
func zoom(level: int, delta: float) -> void:
	var zoom_final = camera.size - (level * delta * MOVEMENT_SPEED)
	if zoom_final >= ZOOM_LOW || zoom_final <= ZOOM_HI:
		camera.size = zoom_final
