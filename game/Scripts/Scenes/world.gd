extends Node

const MAP_GENERATOR = preload("res://addons/map_generator_plugin/services/map_generator.gd")

onready var camera_controler:  = $CameraController
onready var grid_map = $Map/GridMap
onready var data_filename = owner.filename + ".dat"

var map_generator: MapGenerator

func _ready():
	camera_controler.camera.size = 10
	var map_center: Vector3 = grid_map.map_to_world(
		0, 
		camera_controler.camera.size / 2, 
		10)
	camera_controler.translation = map_center
	
	map_generator = MAP_GENERATOR.new(grid_map)
	if not Engine.is_editor_hint():
		load_data()
		map_generator.recalculate_street_density()
		map_generator.generate_map()
	
func load_data() -> void:
	var file = File.new()
	if not file.file_exists(data_filename):
		return
	else:
		var load_state = file.open(data_filename, File.READ)
		if load_state == OK:
			set_data(parse_json(file.get_line()))
		else:
			print("Error opening the file")
	file.close()

func set_data(data_dict: Dictionary) -> void:
	map_generator.map_seed = data_dict["map_seed"]
	map_generator.height = data_dict["height"]
	map_generator.width = data_dict["width"]
	map_generator.street_density = data_dict["street_density"]
	map_generator.erase_fraction = data_dict["erase_fraction"]
	map_generator.buildings_density = data_dict["buildings_density"]
