tool
extends Control

# Constants
# ------------------------------------------------------------------------------

const MAP_GENERATOR = preload("../services/map_generator.gd")

# Variables
# ------------------------------------------------------------------------------

var map_generator: MapGenerator
var map_data_filename: String
var editor_config_filename: String
var initialized: bool = false

onready var map_seed = $Seed/SeedRange
onready var height = $Height/HeightRange
onready var width = $Width/WidthRange
onready var erase_fraction = $EraseFraction/EraseFractionSlider
onready var street_density = $StreetDensity/StreetDensitySlider
onready var buildings_density = $BuildingDensity/BuildingDensitySlider
onready var auto_save_toggle = $AutoSave/AutoSaveToggle
onready var save_btn = $SaveMap


# Methods
# ------------------------------------------------------------------------------

func init(grid_map: GridMap) -> void:
	map_generator = MAP_GENERATOR.new(grid_map)
	map_data_filename = grid_map.owner.filename + ".dat"
	editor_config_filename = grid_map.owner.filename + ".editor.conf"
	
func _ready():
	var map_data = load_file(map_data_filename)
	var editor_config = load_file(editor_config_filename)
	
	if not map_data:
		set_default_map_data()
		save_map()
	else:
		set_map_data(map_data)
	
	if not editor_config:
		set_default_editor_config()
		save_editor_config()
	else:
		set_editor_config(editor_config)
	initialized = true
	map_generator.recalculate_street_density()
	generate_map_if_intialized()

func load_file(filename: String)->Dictionary:
	var file = File.new()
	var result: Dictionary
	if file.file_exists(filename):
		var load_state = file.open(filename, File.READ)
		if load_state == OK:
			result = parse_json(file.get_line())
		else:
			print("Error opening the file")
	file.close()
	return result


func save_file(data_dict: Dictionary, filename: String) -> void:
	var file = File.new()
	var load_state = file.open(filename, File.WRITE)
	if load_state == OK:
		file.store_line(to_json(data_dict))
	else:
		print("Error saving the file")
	file.close()
	
func generate_map_if_intialized() -> void:
	if initialized:
		map_generator.generate_map()
		
func save_map()->void:
	var save_data: Dictionary = {
		"map_seed": map_seed.value,
		"height": height.value,
		"width": width.value,
		"erase_fraction": erase_fraction.value,
		"street_density": street_density.value,
		"buildings_density": buildings_density.value
	}
	save_file(save_data, map_data_filename)
	
func save_editor_config()->void:
	var editor_data: Dictionary = {
		"autosave_is_on": auto_save_toggle.pressed
	}
	save_file(editor_data, editor_config_filename)
	
func try_autosave()->void:
	if auto_save_toggle.pressed:
		save_map()

func set_default_map_data() -> void:
	map_seed.set_value(1)
	height.set_value(10)
	width.set_value(10)
	erase_fraction.set_value(0.8)
	street_density.set_value(2)

func set_default_editor_config()->void:
	auto_save_toggle.set("pressed", false)

func set_map_data(data_dict: Dictionary)->void:
	map_seed.set_value(data_dict["map_seed"])
	height.set_value(data_dict["height"])
	width.set_value(data_dict["width"])
	street_density.set_value(data_dict["street_density"])
	erase_fraction.set_value(data_dict["erase_fraction"])
	buildings_density.set_value(data_dict["buildings_density"])
	
	map_generator.erase_fraction = erase_fraction.value
	map_generator.street_density = street_density.value
	
func set_editor_config(data_dict: Dictionary)->void:
	auto_save_toggle.set("pressed", data_dict["autosave_is_on"])
# Signal Handlers
# ------------------------------------------------------------------------------

func _on_SeedRange_value_changed(value):
	map_generator.map_seed = value
	generate_map_if_intialized()
	try_autosave()

func _on_HeightRange_value_changed(value):
	map_generator.height = value
	generate_map_if_intialized()
	try_autosave()

func _on_WidthRange_value_changed(value):
	map_generator.width = value
	generate_map_if_intialized()
	try_autosave()

func _on_EraseFractionSlider_value_changed(value):
	map_generator.erase_fraction = value
	generate_map_if_intialized()
	try_autosave()

func _on_StreetDensitySlider_value_changed(value):
	map_generator.street_density = value
	map_generator.recalculate_street_density()
	generate_map_if_intialized()
	try_autosave()

func _on_BuildingDensitySlider_value_changed(value):
	map_generator.buildings_density = value
#	map_generator.recalculate_buildings_density()
	generate_map_if_intialized()
	try_autosave()

func _on_AutoSaveToggle_toggled(button_pressed):
	save_btn.disabled = button_pressed
	save_editor_config()
	save_map()

func _on_SaveMap_pressed():
	save_map()
