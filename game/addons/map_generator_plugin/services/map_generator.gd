tool
extends Node
class_name MapGenerator

# Constants
# ------------------------------------------------------------------------------
const MAX_STREET_DENSITY = 4

const N: int = 0x1
const E: int = 0x2
const S: int = 0x4
const W: int = 0x8

# Variables
# ------------------------------------------------------------------------------
var width: int = 10
var height: int = 10
var map_seed: int = 0
var erase_fraction: float = 0.8
var street_density: int = 2
var buildings_density: int = 0.8

var grid_map: GridMap
var cell_walls: Dictionary

var restricted_positions: Array
var street_positions: Array

# Overrides
# ------------------------------------------------------------------------------
func _init(p_grid_map: GridMap):
	grid_map = p_grid_map
	randomize()

# Methods
# ------------------------------------------------------------------------------
func recalculate_street_density()->void:
	street_density = MAX_STREET_DENSITY - street_density + 1
	cell_walls = {
		Vector3(0, 0, -street_density): N,
		Vector3(street_density, 0, 0) : E,
		Vector3(0, 0, street_density) : S,
		Vector3(-street_density, 0, 0): W 
	}

func generate_map() -> void:
	if !is_instance_valid(grid_map):
		push_warning("grid map object is null, nothing will be generated")
		return
	
	var unvisited = init_map()
	var cell_stack = []
	var curr_pos = Vector3(0, 0, 0)
	
	unvisited.erase(curr_pos)

	while unvisited:
		var neighbors = get_neighbors(curr_pos, unvisited)
		if neighbors.size() > 0:
			var next_pos = neighbors[randi() % neighbors.size()]
			cell_stack.append(curr_pos)
			
			var dir = next_pos - curr_pos
			var curr_map_pos = grid_map.world_to_map(curr_pos)
			var next_map_pos = grid_map.world_to_map(next_pos)
			var curr_walls = get_cell_item(curr_map_pos) - cell_walls[dir]
			var next_walls = get_cell_item(next_map_pos) - cell_walls[-dir]
			
			set_cell_item(curr_map_pos, curr_walls)
			set_cell_item(next_map_pos, next_walls)

			if street_density > 1:
				for i in range(1, street_density):
					set_cell_item(
						curr_map_pos + dir / street_density * i, 
						N | S if dir.x != 0 else E | W)

			curr_pos = next_pos
			unvisited.erase(curr_pos)
			restricted_positions.erase(curr_map_pos)
			street_positions.append(curr_map_pos)
			
		elif cell_stack:
			curr_pos = cell_stack.pop_back()
			
	erase_random_walls()
#	generate_buildings()

func erase_random_walls() -> void:
	for i in range(int(width * height * erase_fraction)):
		var x = int(rand_range(1, width / street_density)) * street_density
		var z = int(rand_range(1, height / street_density)) * street_density
		
		var curr_pos = Vector3(x, 0, z)
		var next_dir = cell_walls.keys()[randi() % cell_walls.size()]
		var curr_idx = get_cell_item(curr_pos)
		
		if curr_idx & cell_walls[next_dir]:
			var next_pos = curr_pos + next_dir
			var next_idx = get_cell_item(next_pos)
			
			set_cell_item(curr_pos, curr_idx - cell_walls[next_dir])
			set_cell_item(next_pos, next_idx - cell_walls[-next_dir])
			restricted_positions.erase(curr_pos)
			street_positions.append(curr_pos)

			if street_density > 1:
				for j in range(1, street_density):
					var cell_pos = curr_pos + (next_dir.normalized() * j)
					set_cell_item(cell_pos, N | S if next_dir.x != 0 else E | W)
					restricted_positions.erase(cell_pos)
					street_positions.append(cell_pos)

func generate_buildings()->void:
	for res_pos in restricted_positions:
		var building = 16
		set_cell_item(res_pos, building)

func init_map() -> Array:
	grid_map.clear()
	restricted_positions.clear()
	street_positions.clear()
	
	if !map_seed:
		map_seed = randi()
	seed(map_seed)
	
	var unvisited = []
	
	for x in range(width):
		for z in range(height):
			var cell_pos = Vector3(x, 0, z)
			set_cell_item(cell_pos, N|E|S|W)
			restricted_positions.append(cell_pos)
			
			if street_density > 1:
				if x % street_density == 0 and z % street_density == 0:
					unvisited.append(cell_pos)
			else:
				unvisited.append(cell_pos)
	return unvisited

func get_neighbors(cell_pos: Vector3, unvisited: Array)->Array:
	var result = []
	for n_dir in cell_walls.keys():
		var n_pos = cell_pos + n_dir
		if n_pos in unvisited:
			result.append(n_pos)
	return result

func set_cell_item(pos: Vector3, type: int)->void:
	grid_map.set_cell_item(pos.x, pos.y, pos.z, type)
		
func get_cell_item(pos: Vector3) -> int:
	return grid_map.get_cell_item(pos.x, pos.y, pos.z)
