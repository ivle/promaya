#include <MapGenerator.hpp>
#include <Vector3.hpp>
#include <stack>
#include <bits/stdc++.h>
#include <algorithm>
#include <cstdlib>

namespace godot
{
    void MapGenerator::_register_methods()
    {
        register_method("SetGridMap", &MapGenerator::SetGridMap);
        register_method("GenerateMap", &MapGenerator::GenerateMap);
        register_method("SetWidth", &MapGenerator::SetWidth);
        register_method("SetHeight", &MapGenerator::SetHeight);
        register_method("SetSeed", &MapGenerator::SetSeed);
        register_method("SetEraseFraction", &MapGenerator::SetEraseFraction);
        register_method("SetStreetDensity", &MapGenerator::SetStreetDensity);
    }

    void MapGenerator::_init()
    {
        UpdateMembers();
    }

    void MapGenerator::_process(float delta)
    { }

    void MapGenerator::GenerateMap()
    {
        std::vector<Vector3> unvisited;
        std::stack<Vector3> cellStack;

        Vector3 current = Vector3(0, 0, 0);
        m_gridMap->clear();

        for (int x = 0; x < m_width; x++)
        {
            for (int z = 0; z < m_height; z++)
            {
                if (m_streetDensity == 1)
                {
                    unvisited.push_back(Vector3(x, 0, z));
                }

                m_gridMap->set_cell_item(x, 0, z, N | E | S | W );
            }
        }

        if (m_streetDensity > 1)
        {
            for (int x = 0; x < m_width; x += m_streetDensity)
            {
                for (int z = 0; z < m_height; z += m_streetDensity)
                {
                    unvisited.push_back(Vector3(x, 0, z));
                }      
            }
        }

        remove(unvisited.begin(), unvisited.end(), current);

        while(!unvisited.empty())
        {
            std::vector<Vector3> neighbors = GetUnvisitedNeighbors(current, unvisited);

            if (neighbors.size() > 0)
            {
                Vector3 next = neighbors[rand() % neighbors.size()];
                Vector3 dir = next - current;

                cellStack.push(current);

                Vector3 currentMapPos = m_gridMap->world_to_map(current);
                Vector3 nextMapPos = m_gridMap->world_to_map(next);

                int currentMeshIdx = GetCellItem(currentMapPos);
                int nextMeshIdx = GetCellItem(nextMapPos);

                int currentWalls = currentMeshIdx - m_cellWalls.at(dir);
                int nextWalls = nextMeshIdx - m_cellWalls.at(-dir);

                SetCellItem(currentMapPos, currentWalls);
                SetCellItem(nextMapPos, nextWalls);

                if (m_streetDensity > 1)
                {
                    for (int i = 1; i < m_streetDensity; i++)
                    {
                        int carinaldDir = dir.x != 0 ? N | S : E | S;
                        Vector3 fillCell = currentMapPos + dir / m_streetDensity * i;
                        SetCellItem(fillCell, carinaldDir);
                    }
                }

                current = next;
                remove(unvisited.begin(), unvisited.end(), current);
            }
            else if (!cellStack.empty())
            {
                current = cellStack.top();
                cellStack.pop();
            }
        }
    }

    void MapGenerator::EraseRandomWalls()
    {
        int totalSize = m_width * m_height * m_eraseFraction;
        
        for (int i = 0; totalSize; i++)
        {
            int x = ((rand() % m_width / 2 - 2) + 2) * 2;
            int z = ((rand() % m_height / 2 - 2) + 2) * 2;

            Vector3 cellPos = Vector3(x, 0, z);
            int cellIdx = GetCellItem(cellPos);

            Vector3 nextPos = GetRandomDirection();

            if(cellIdx & m_cellWalls.at(nextPos))
            {
                int nextIdx = GetCellItem(cellPos + nextPos);

                int cellWalls = cellIdx - m_cellWalls.at(nextPos);
                int nextWalls = nextIdx - m_cellWalls.at(-nextPos);

                SetCellItem(cellPos, cellWalls);
                SetCellItem(nextPos, nextWalls);
            }

            if (m_streetDensity > 1)
            {
                for (int j = 1; j < m_streetDensity; j++)
                {
                    int carinaldDir = nextPos.x != 0 ? N | S : E | S;
                    Vector3 fillCell = cellPos + nextPos / m_streetDensity * i;
                    SetCellItem(fillCell, carinaldDir);
                }
            }
        }
    }

    std::vector<Vector3> MapGenerator::GetUnvisitedNeighbors(
        Vector3 cell, 
        const std::vector<Vector3> unvisited)
    {
        std::vector<Vector3> neighbors;

        for (auto const& side: m_cellWalls)
        {
            Vector3 neighbor = cell + side.first * m_streetDensity;

            if(std::count(unvisited.begin(), unvisited.end(), neighbor))
            {
                neighbors.push_back(neighbor);
            }
        }

        return neighbors;
    }

    void MapGenerator::UpdateMembers()
    {
        srand(m_seed);
    }

    inline int MapGenerator::GetCellItem(Vector3 position)
    {
        return m_gridMap->get_cell_item(position.x, position.y, position.z);
    }

    inline void MapGenerator::SetCellItem(Vector3 position, int item)
    {
        m_gridMap->set_cell_item(position.x, position.y, position.z, item);
    }

    inline Vector3 MapGenerator::GetRandomDirection()
    {
        return std::next(std::begin(m_cellWalls), rand() % m_cellWalls.size())->first;
    }
}