#ifndef MAP_GENERATOR
#define MAP_GENERATOR

#include <Godot.hpp>
#include <Node.hpp>
#include <GridMap.hpp>
#include <vector>
#include <map>

namespace godot
{
    class MapGenerator: public Node
    {
        GODOT_CLASS(MapGenerator, Node);
        
        private:
        const int N = 0x01;
        const int E = 0x02;
        const int S = 0x04;
        const int W = 0x08;
        
        const std::map<Vector3, int> m_cellWalls = 
        {
            { Vector3( 0, 0, -1), N },
            { Vector3( 1, 0,  0), E },
		    { Vector3( 0, 0,  1), S },
		    { Vector3(-1, 0,  0), W } 
        };

        GridMap* m_gridMap;

        private:
        int m_width;
        int m_height;
        int m_seed;
        int m_eraseFraction;
        int m_streetDensity;

        public:
            MapGenerator() { }
            ~MapGenerator() = default;

            static void _register_methods();
            void _init();
            void _process(float delta);

            void SetGridMap(GridMap* gridMap) { m_gridMap = gridMap; }            
            void SetWidth(int width) { m_width = width; }
            void SetHeight(int height) { m_height = height; }
            void SetSeed(int seed) { m_seed = seed; }
            void SetEraseFraction(float fraction) { m_eraseFraction = fraction; }
            void SetStreetDensity(int density ) { m_streetDensity = density; }
            void GenerateMap();

        private:
            std::vector<Vector3> GetUnvisitedNeighbors(
                Vector3 cell, 
                const std::vector<Vector3> unvisited);
            void EraseRandomWalls();
            void UpdateMembers();

            inline int GetCellItem(Vector3 position);
            inline void SetCellItem(Vector3 position, int item);
            inline Vector3 GetRandomDirection();
    };
}

#endif