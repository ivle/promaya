tool
extends EditorInspectorPlugin

# Preloads
# ------------------------------------------------------------------------------
const INSPECTOR = preload("ui/inspector_dock.tscn")

# Variables
# ------------------------------------------------------------------------------
var inspector

func can_handle(object):
	return object is GridMap

func parse_begin(object):
	if object is GridMap:
		inspector = INSPECTOR.instance()
		inspector.init(object)
		add_custom_control(inspector)
		return true
	else:
		return false
