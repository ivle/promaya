tool
extends EditorPlugin

# Variables
# ------------------------------------------------------------------------------
var dock
var inspector

func _enter_tree():
	inspector = preload("inspector_plugin.gd").new()
	add_inspector_plugin(inspector)

func _exit_tree():
	remove_inspector_plugin(inspector)
