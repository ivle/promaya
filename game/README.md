 
# Promaya
____

Game description goes here

## Getting the source code and setting up the project

Before setting up the project there are a few prerequisites you'll need:

    a Godot 3.x executable,
    a C++ compiler,
    SCons as a build tool,

Clone with SSH:

``` bash
git clone --recurse-submodules -j8 repo
```
If you don't have ssh already set up visit 
[link](https://www.freecodecamp.org/news/git-ssh-how-to/) for more information on how to set up.

Or alternatively clone with HHTPS:

```bash
git clone --recurse-submodules -j8 repo
```

The repository depends on the [link](https://github.com/GodotNativeTools/godot-cpp)
repo as a submodule, so remeber to clone it recursevily.

If you didn't clone the repo with the `--recurse-submodules` flag `cd` to the project folder
and run:

``` bash
git submodule update --init --recursive
```

## Building the C++ bindings

Now that we've downloaded our prerequisites, it is time to build the C++ bindings.
First run the command bellow to generate the metadata for building the bindings:

``` bash
godot --gdnative-generate-json-api api.json
```

Place the resulting `api.json` file in the project folder and run:

``` bash
scons platform=linux generate_bindings=yes use_custom_api_file=yes custom_api_file=../api.json -j4
```

where platform can be `linux`, `windows`, or `osx`.
