 
# Promaya
____

Game description goes here

## Setting up the project
____

### Cloning the repository

To clone it with SSH, run:

``` bash
git clone --recurse-submodules -j8 repo
```
Or alternatively clone with HHTPS:

```bash
git clone --recurse-submodules -j8 repo

```
:exclamation: <strong>Some of the features/plugins for this project are written in C/C++ using NativeScript. 
In order to modify them you need to clone the repo recursevily to get the submodules (as shown above)</strong>

If you didn't clone the repo with the `--recurse-submodules` flag, `cd` to the project folder
and run:

``` bash
git submodule update --init --recursive
```
____

### NativeScript Prerequisites

Before buiding the project you need to download the following prerequisites:

    a Godot 3.x executable,
    a C++ compiler,
    SCons as a build tool,

____

### Building the C++ bindings

Now that we've downloaded our prerequisites, it is time to build the C++ bindings.

:exclamation: <strong>You can ommit the following commands, as the files are already commited
but I will leave them here just in case something goes wrong</strong>

First run the command bellow to generate the metadata for building the bindings:

``` bash
godot --gdnative-generate-json-api api.json
```

Place the resulting `api.json` file in the project folder and run:

``` bash
scons platform=linux generate_bindings=yes use_custom_api_file=yes custom_api_file=../api.json -j4
```

where platform can be `linux`, `windows`, or `osx`.
